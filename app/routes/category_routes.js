const Category = require('../models/category.model');
const express = require('express');
const { ReplSet } = require('mongodb');
const router = express.Router();
router.get('/',async (req,res) => {
    try{
        const categories = await Category.find();
        res.json(categories);
    }catch(err){
        res.json({'message':err});
    }
});
router.get('/:id',async (req,res) => {
    try{
        const category = await Category.findById(req.params.id);
        res.json(category);
    }catch(err){
        res.json({'message':err});
    }
});
router.delete('/:id',async (req,res) => {
    const id = req.params.id;
    try{
        const removedCategory = await Category.remove({'_id':id});
        res.json(removedCategory);
    }catch(err){
        res.json({'message':err});
    }
});
router.patch('/:id',async (req,res) => {
    const id = req.params.id;
    const details = {'_id':id};
    try{
        const updatedCategory = await Category.updateOne(
            details,
            {$set:{
                    name: req.body.name
                }
            },
            {
                upsert:true
            }
        );
        res.json(updatedCategory);
    }
    catch(err){
        res.json({'message':err});
    }
});
router.post('/',async (req,res) => {
    const category = new Category({
        name: req.body.name
    });
    try{
        const savedCategory = await category.save();
        res.json(savedCategory);
    } catch(err){
        res.json({'message':err});
    }
});
module.exports = router;
const express = require('express');
const router = express.Router();
const User = require('../models/user.model');
const {auth} = require('../auth');

// adding new user (sign-up route)
router.post('/register',function(req,res){
    const newuser=new User(req.body);
    newuser.save((err,doc)=>{
        if(err) {console.log(err);
            return res.status(400).json({ success : false});}
        res.status(200).json({
            success:true,
            user : doc
        });
    });
 });
  
router.post('/login',(req,res)=>{
    let token=req.cookies.auth;
    User.findByToken(token,(err,user)=>{
        if(err) return  res(err);
        if(user) return res.status(400).json({
            error :true,
            message:"You are already logged in"
        });
    
        else{
            User.findOne({'email':req.body.email},function(err,user){
                if(!user) return res.json({isAuth : false, message : 'Auth failed! E-mail not found'});
                user.comparepassword(req.body.password,(err,isMatch)=>{
                    if(!isMatch) return res.json({ isAuth : false,message : "Password doesn't match"});
                user.generateToken((err,user)=>{
                    if(err) return res.status(400).send(err);
                    res.cookie('auth',user.token).json({
                        isAuth : true,
                        id : user._id,
                        email : user.email,
                        name:user.name
                    });
                });    
            });
          });
        }
    });
});

// get logged in user
router.get('/profile',auth,function(req,res){
    res.json({
        isAuth: true,
        id: req.user._id,
        email: req.user.email,
        name: req.user.name
    })
});

//logout user
router.get('/logout',auth,function(req,res){
    req.user.deleteToken(req.token,(err,user)=>{
        if(err) return res.status(400).send(err);
        res.sendStatus(200);
    });
}); 

module.exports = router;
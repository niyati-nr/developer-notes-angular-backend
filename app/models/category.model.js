const mongoose = require('mongoose');
const BlogPost = require('./blogpost.model');
const CategorySchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique:true
    }
})

module.exports = mongoose.model('Categories', CategorySchema);